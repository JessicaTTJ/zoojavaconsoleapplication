package com.ing.zoo.animals;

import com.ing.zoo.Animal;
import com.ing.zoo.visitor.AnimalVisitor;
import com.ing.zoo.visitor.Meat;

import java.util.Random;

public class Tiger extends Animal {

    public Tiger()
    {
        setName(" wally");
    }

    @Override
    public void accept(AnimalVisitor visitor) {
        visitor.visit(this);
    }

    public void sayHello()
    {
        setHelloText("Tiger Wally says: rraaarww");
        System.out.println(getHelloText());
    }

    public void eatMeat(Meat meat)
    {
        setEatText("Tiger Wally says: nomnomnom oink wubalubadubdub");
        System.out.println(getEatText());
    }
    @Override
    public void performTrick()
    {
        Random random = new Random();
        int rnd = random.nextInt(2);
        if(rnd == 0)
        {
            setTrick("Tiger Wally: jumps in tree");
        }
        else
        {
            setTrick("Tiger Wally: scratches ears");
        }
        System.out.println(getTrick());
    }
}
