package com.ing.zoo.animals;

import com.ing.zoo.Animal;
import com.ing.zoo.visitor.AnimalVisitor;
import com.ing.zoo.visitor.Leaves;
import com.ing.zoo.visitor.Meat;

import java.util.Random;

public class Monkey extends Animal {

    public Monkey()
    {
        setName(" mankey");
    }

    @Override
    public void accept(AnimalVisitor visitor) {
        visitor.visit(this);
    }

    public void sayHello()
    {
        setHelloText("Monkey Mankey says: oe oe a aa");
        System.out.println(getHelloText());
    }

    public void eatLeaves(Leaves leaf)
    {
        setEatText("Monkey Mankey says: aaaaaaa aaaa oe oe");
        System.out.println(getEatText());
    }

    public void eatMeat(Meat meat)
    {
        setEatText("Monkey Mankey says: ook ook ook thx");
        System.out.println(getEatText());
    }
    @Override
    public void performTrick()
    {
        Random random = new Random();
        int rnd = random.nextInt(2);
        if(rnd == 0)
        {
            setTrick("Monkey Mankey: karate chop");
        }
        else
        {
            setTrick("Monkey Mankey: low kick");
        }
        System.out.println(getTrick());
    }
}
