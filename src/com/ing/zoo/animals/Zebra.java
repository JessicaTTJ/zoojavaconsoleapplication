package com.ing.zoo.animals;

import com.ing.zoo.Animal;
import com.ing.zoo.visitor.AnimalVisitor;
import com.ing.zoo.visitor.Leaves;

public class Zebra extends Animal {

    public Zebra()
    {
        setName(" marty");
    }

    @Override
    public void accept(AnimalVisitor visitor) {
        visitor.visit(this);
    }

    public void sayHello()
    {
        setHelloText("Zebra Marty says: zebra zebra");
        System.out.println(getHelloText());
    }

    public void eatLeaves(Leaves leaf)
    {
        setEatText("Zebra Marty says: munch munch zank yee bra");
        System.out.println(getEatText());
    }
}
