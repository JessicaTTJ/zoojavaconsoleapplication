package com.ing.zoo.animals;

import com.ing.zoo.Animal;
import com.ing.zoo.visitor.AnimalVisitor;
import com.ing.zoo.visitor.Meat;

public class Lion extends Animal {

    public Lion()
    {
        super();
        setName(" henk");
    }

    @Override
    public void accept(AnimalVisitor visitor) {
        visitor.visit(this);
    }

    public void sayHello()
    {
        setHelloText("Lion Henk says: roooaoaaaaar");
        System.out.println(getHelloText());
    }

    public void eatMeat(Meat meat)
    {
        setEatText("Lion Henk says: nomnomnom thx mate");
        System.out.println(getEatText());
    }
}
