package com.ing.zoo.animals;

import com.ing.zoo.Animal;
import com.ing.zoo.visitor.AnimalVisitor;
import com.ing.zoo.visitor.Leaves;

public class Hippo extends Animal {

    public Hippo()
    {
        setName(" elsa");
    }

    @Override
    public void accept(AnimalVisitor visitor) {
        visitor.visit(this);
    }

    public void sayHello()
    {
        setHelloText("Hippo Elsa says: splash");
        System.out.println(getHelloText());
    }

    public void eatLeaves(Leaves leaf)
    {
        setEatText("Hippo Elsa says: munch munch lovely");
        System.out.println(getEatText());
    }
}
