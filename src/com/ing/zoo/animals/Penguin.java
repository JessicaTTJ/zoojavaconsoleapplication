package com.ing.zoo.animals;

import com.ing.zoo.Animal;
import com.ing.zoo.visitor.AnimalVisitor;
import com.ing.zoo.visitor.Meat;

public class Penguin extends Animal {

    public Penguin()
    {
        setName(" piplup");
    }

    @Override
    public void accept(AnimalVisitor visitor) {
        visitor.visit(this);
    }

    public void sayHello()
    {
        setHelloText("Penguin Piplup says: weeeee");
        System.out.println(getHelloText());
    }

    public void eatMeat(Meat meat)
    {
        setEatText("Penguin Piplup says: gakgakgak yeet");
        System.out.println(getEatText());
    }
}
