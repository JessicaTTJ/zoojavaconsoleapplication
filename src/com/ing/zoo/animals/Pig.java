package com.ing.zoo.animals;

import com.ing.zoo.Animal;
import com.ing.zoo.visitor.AnimalVisitor;
import com.ing.zoo.visitor.Leaves;
import com.ing.zoo.visitor.Meat;

import java.util.Random;

public class Pig extends Animal {

    public Pig()
    {
        setName(" dora");
    }

    @Override
    public void accept(AnimalVisitor visitor) {
        visitor.visit(this);
    }

    public void sayHello()
    {
        setHelloText("Pig Dora says: splash");
        System.out.println(getHelloText());
    }

    public void eatLeaves(Leaves leaf)
    {
        setEatText("Pig Dora says: munch munch oink");
        System.out.println(getEatText());
    }

    public void eatMeat(Meat meat)
    {
        setEatText("Pig Dora says: nomnomnom oink thx");
        System.out.println(getEatText());
    }
    @Override
    public void performTrick()
    {
        Random random = new Random();
        int rnd = random.nextInt(2);
        if(rnd == 0)
        {
            setTrick("Pig Dora: rolls in the mud");
        }
        else
        {
            setTrick("Pig Dora: runs in circles");
        }
        System.out.println(getTrick());
    }
}
