package com.ing.zoo.visitor;

import com.ing.zoo.animals.*;

/**
 * This interface visits each animal class. The animal subclasses need to accept the visitor first by implementing
 * the accept() method from the Animal superclass.
 * Whenever a new animal is added to the zoo and the food type needs to be determined, add the animal here.
 */
public interface AnimalVisitor {
    void visit(Hippo hippo);
    void visit(Lion lion);
    void visit(Pig pig);
    void visit(Tiger tiger);
    void visit(Zebra zebra);
    void visit(Penguin penguin);
    void visit(Monkey monkey);
}
