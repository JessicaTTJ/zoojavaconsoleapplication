package com.ing.zoo.visitor;

/**
 * A concrete visitor implementing the AnimalVisitor interface.
 * The Meat and Leaves class can be derived from this class to determine the type of food an animal eats.
 */
public abstract class Food implements AnimalVisitor {
    public void visit(AnimalVisitor animalVisitor) {
    }
}
