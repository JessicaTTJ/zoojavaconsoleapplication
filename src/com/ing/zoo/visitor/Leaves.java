package com.ing.zoo.visitor;

import com.ing.zoo.animals.*;

/**
 * A concrete visitor based off the AnimalVisitor interface.
 * After the animal subclasses implement the accept() method and accept the visitor (interface), these operations are
 * used to determine which animal eats leaves.
 */
public class Leaves extends Food {
    @Override
    public void visit(Hippo hippo) {
        hippo.eatLeaves(this);
    }
    @Override
    public void visit(Zebra zebra) {
        zebra.eatLeaves(this);
    }
    @Override
    public void visit(Pig pig) {
        // actually an omnivore
        pig.eatLeaves(this);
    }
    @Override
    public void visit(Monkey monkey){
        // actually an omnivore
        monkey.eatLeaves(this);
    }

    public void visit(Lion lion) {
        // not a herbivore
    }

    public void visit(Tiger tiger) {
        // not a herbivore
    }

    public void visit(Penguin penguin){
        // not a herbivore
    }
}
