package com.ing.zoo.visitor;

import com.ing.zoo.animals.*;
/**
 * A concrete visitor based off the AnimalVisitor interface.
 * After the animal subclasses implement the accept() method and accept the visitor (interface), these operations are
 * used to determine which animal eats meat.
 */
public class Meat extends Food {
    @Override
    public void visit(Lion lion) {
        lion.eatMeat(this);
    }
    @Override
    public void visit(Tiger tiger) {
        tiger.eatMeat(this);
    }
    @Override
    public void visit(Penguin penguin){
        penguin.eatMeat(this);
    }
    @Override
    public void visit(Monkey monkey){
        monkey.eatMeat(this);
    }
    @Override
    public void visit(Pig pig) {
        // actually an omnivore
        pig.eatMeat(this);
    }

    public void visit(Hippo hippo) {
        // not a carnivore
    }

    public void visit(Zebra zebra) {
        // not a carnivore
    }
}
