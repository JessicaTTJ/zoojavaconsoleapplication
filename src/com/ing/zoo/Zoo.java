package com.ing.zoo;

import com.ing.zoo.animals.*;
import com.ing.zoo.visitor.Food;
import com.ing.zoo.visitor.Leaves;
import com.ing.zoo.visitor.Meat;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * This Zoo program implements an application that displays the result of a given command.
 * The commands are: hello, hello [animal name], give leaves, give meat and perform trick.
 */
public class Zoo {

    public static List<Animal> animals = new ArrayList<>();

    public void addAnimal(Animal animal) {
        animals.add(animal);
    }

    public void acceptFood(Food food) {
        for (Animal animal : animals) {
            animal.accept(food);
        }
    }

    public static void main(String[] args)
    {
        String[] commands = new String[4];
        commands[0] = "hello";
        commands[1] = "give leaves";
        commands[2] = "give meat";
        commands[3] = "perform trick";

        Zoo zoo = new Zoo();
        zoo.addAnimal(new Hippo());
        zoo.addAnimal(new Lion());
        zoo.addAnimal(new Pig());
        zoo.addAnimal(new Tiger());
        zoo.addAnimal(new Zebra());
        // Added 2 new animals
        zoo.addAnimal(new Penguin());
        zoo.addAnimal(new Monkey());

        System.out.println("Welkom in de dierentuin! Dit zijn de commands om met de dieren te spelen:" + "\nHello | Hello [Elsa/Henk/Dora/Wally/Marty/Piplup/Mankey] | Give Leaves | Give Meat | Perform Trick");
        Scanner scanner = new Scanner(System.in);
        System.out.print("Voer uw command in: ");

        String input = scanner.nextLine();

        for (Animal animal : animals) {
            // if you say hello to a specific animal, only that animal greets you back
            if(input.equalsIgnoreCase(commands[0] + animal.getName()))
            {
                animal.sayHello();
            }
            // if you just say hello, every animal greets you back
            else if(input.equalsIgnoreCase(commands[0])){
                animal.sayHello();
            }

            // give leaves to all herbivores/omnivores
            if(input.equalsIgnoreCase(commands[1])){
                zoo.acceptFood(new Leaves());
                break;
            }
            // give meat to all carnivores/omnivores
            if(input.equalsIgnoreCase(commands[2])){
                zoo.acceptFood(new Meat());
                break;
            }

            // animals that can perform a trick, will do so
            if(input.equalsIgnoreCase(commands[3])){
                animal.performTrick();
            }
        }
    }
}
