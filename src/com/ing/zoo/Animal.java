package com.ing.zoo;

import com.ing.zoo.visitor.AnimalVisitor;

/**
 * This class serves as the superclass for all zoo animals.
 */
public abstract class Animal {
    private String name;
    private String helloText;
    private String eatText;
    private String trick;

    public void setName(String aName){name = aName;}
    public String getName() {return name;}

    public void setHelloText(String aText) {helloText = aText;}
    public String getHelloText(){return helloText;}

    public void setEatText(String eatTxt) {eatText = eatTxt;}
    public String getEatText(){return eatText;}

    public void setTrick(String aTrick){trick = aTrick;}
    public String getTrick(){return trick;}

    public void sayHello(){
        // the animals say hi, can't be an abstract method due to package structure
    }

    public void performTrick(){
        // the animals that can perform tricks, implement this method
    }

    /**
     * This method is part of the Visitor Pattern to check if an animal eats meat, leaves or both.
     * @param visitor part of AnimalVisitor interface that holds all animal objects.
     *                Each animal subclass needs to accept() the visitor so it can execute the operations
     *                in either the Meat or Leaves class. This way, in the main program, the animals that eat
     *                leaves or meat are divided (as described in the Meat/Leaves class) so each animal responds to the correct
     *                'give leaves' and 'give meat' command.
     */
    public abstract void accept(AnimalVisitor visitor);
}
