﻿# Zoo Java Console Application
---
## Git

Voor het maken van deze opdracht wordt er gebruik gemaakt van het versiebeheersysteem Git.

Gebruikte **IDE**: [IntelliJ IDEA](https://www.jetbrains.com/idea/).

Gebruikte **SDK**: [JDK 1.8](https://bitbucket.org/kychu/zoo/downloads/java-1.8.0-openjdk-1.8.0.212-3.b04.redhat.windows.x86_64.zip).

--- 
## Klassendiagram

[Link naar klassendiagram](https://cdn.discordapp.com/attachments/616381197139837033/717455157813837905/Zoo.png "Klassediagram")

---
## Eisen

Bij elke command laat je het resultaat in de console zien.

- Als je de [hello] command uitvoert zonder dat er een naam is ingevuld zeggen alle dieren hallo.
![Hello cmd](https://cdn.discordapp.com/attachments/616381197139837033/717128555254906890/unknown.png)

	- Als er wel een naam is ingevuld [hello henk] zegt alleen dat dier hallo 
	
		![Hello henk](https://cdn.discordapp.com/attachments/616381197139837033/717128736885047296/unknown.png)
	
- Als je de [give leaves] command uitvoert krijgen alle herbivores leaves.

	![Give Leaves cmd](https://cdn.discordapp.com/attachments/616381197139837033/717128844145852535/unknown.png)
	
- Als je de [give meat] command uitvoert krijgen alle carnivores meat.

	![Give Meat cmd](https://cdn.discordapp.com/attachments/616381197139837033/717128929424572526/unknown.png)
	
- Als je de [perform trick] command uitvoert doen alle dieren die dat kunnen hun trucje.

	![Perform Trick cmd](https://cdn.discordapp.com/attachments/616381197139837033/717129025662746654/unknown.png)
